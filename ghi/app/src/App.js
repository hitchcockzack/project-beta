import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerCreateForm from './ManufacturerCreateForm';
import ManufacturerList from './ManufacturerList';
import VehicleModelList from './VehicleModelList';
import VehicleModelCreate from './VehicleModelCreate';
import TechCreateForm from './TechCreateForm';
import TechList from './TechList';
import TechDelete from './TechDelete';
import AppointmentCreateForm from './AppointmentCreateForm';
import ScheduledAppointments from './ScheduledAppointments';
import ServiceHistory from './ServiceHistory';

import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import SalespersonHistory from './SalespersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="new" element={<ManufacturerCreateForm />} />
          <Route path="manufacturer" element={<ManufacturerList />} />
          <Route path="model" element={<VehicleModelList />} />
          <Route path="/create" element={<VehicleModelCreate />} />
          <Route path="/techcreate" element={<TechCreateForm />} />
          <Route path="/techlist" element={<TechList />} />
          <Route path="/techdelete" element={<TechDelete />} />
          <Route path="/appointmentcreate" element={<AppointmentCreateForm />} />
          <Route path="/scheduledappointments" element={<ScheduledAppointments />} />
          <Route path="/servicehistory" element={<ServiceHistory />} />

          <Route path="automobiles/" element={<AutomobileList />} />
          <Route path="automobiles/create" element={<AutomobileForm />} />
          <Route path="salespeople/" element={<SalespersonList />} />
          <Route path="salespeople/create" element={<SalespersonForm />} />
          <Route path="customers/" element={<CustomerList />} />
          <Route path="customers/create" element={<CustomerForm />} />
          <Route path="sales/" element={<SaleList />} />
          <Route path="sales/create" element={<SaleForm />} />
          <Route path="salespeople/history" element={<SalespersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
