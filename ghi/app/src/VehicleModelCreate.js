import React, { useEffect, useState } from 'react';

function VehicleModelCreate() {
    const [manufacturer, setManufacturer] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        try {
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturer(data.manufacturers);
            } else {
                console.error('Failed to fetch manufacturers:', response.statusText);
            }
        } catch (error) {
            console.error('Error during fetch:', error.message);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const modelsUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelsUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        if (inputName === "manufacturer") {
            setFormData((prevFormData) => ({
                ...prevFormData,
                manufacturer_id: value,
            }));
        } else {
            setFormData((prevFormData) => ({
                ...prevFormData,
                [inputName]: value,
            }));
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="Vehicle Model Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Vehicle Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturer && manufacturer.map(manufacturer => (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelCreate;
