import React, { useEffect, useState } from 'react';

function TechList() {
    const [techs, setTechs] = useState([])

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');

            if (response.ok) {
                const data = await response.json();
                setTechs(data.technicians); // Update to access the 'technicians' array
            } else {
                console.error('Failed to fetch data');
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };
    useEffect(() => {
        getData()
    }, [])
    const handleDelete = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/technicians/${id}/`, {
                method: 'DELETE',
            });

            if (response.ok) {

                getData();
            } else {
                console.error('Failed to remove technician');
            }
        } catch (error) {
            console.error('Error during Technician removal:', error);
        }
    };
    return (
        <div>
            <h1>Remove a Technician</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        <th>System ID</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {techs.map(tech => {
                        return (
                            <tr key={tech.id}>
                                <td>{tech.first_name}</td>
                                <td>{tech.last_name}</td>
                                <td>{tech.employee_id}</td>
                                <td>{tech.id}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => handleDelete(tech.id)}>
                                        Remove
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechList;
