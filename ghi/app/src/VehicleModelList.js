import React, { useEffect, useState } from 'react';

function VehicleModelList() {
    const [models, setModels] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1>List Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vehicle Model Name</th>
                        <th>Picture URL</th>
                        <th>Manufacturer Name</th>
                        <th>Vehicle Model ID</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td><img src={model.picture_url} alt={model.name} style={{ maxWidth: '100px' }} /></td>
                                <td>{model.manufacturer.name}</td>
                                <td>{model.id}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default VehicleModelList;
