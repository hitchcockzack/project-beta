import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


function AutomobileList() {
    const [autos, setAutos] = useState([]);
    const navigate = useNavigate()
    const fetchAutos = async () => {
        const autosApiUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(autosApiUrl);
        if (response.ok) {
            const data = await response.json();


            if (data === undefined) {
                return null;
            }

            setAutos(data.autos)
        }
    }
    useEffect(() => {
        fetchAutos();
    }, [])

    const handleOnClick = () => {
    navigate("/automobiles/create");
}

    return (
    <>
    <div>
      <h1>Automobile List</h1>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Year</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Vin</th>
          </tr>
        </thead>
        <tbody>
          {autos.map(auto => {
            return (
              <tr key={ auto.href }>
                <td>{auto.year}</td>
                <td>{ auto.model.manufacturer.name }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.color }</td>
                <td>{ auto.vin }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div><button onClick={handleOnClick} type="button" className='btn btn-primary' data-bs-toggle="button">Create a new Car?</button></div>
      </div>
      </>
    );
  }

export default AutomobileList;
