import React, { useState } from 'react';

function ManufacturerCreateForm() {
    const [formData, setFromData] = useState({
        name: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const manufacturersURL = ('http://localhost:8100/api/manufacturers/');

        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturersURL, fetchConfig);

        if (response.ok) {
            setFromData({
                name: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFromData({
            ...formData,
            [inputName]: value,
        });

        
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="manufacturer-create-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerCreateForm;
