import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


function CustomerList() {
    const [customer, setCustomers] = useState([]);
    const navigate = useNavigate()

    const fetchCustomers = async () => {
        const customerApiUrl = "http://localhost:8090/api/customers";
        const response = await fetch(customerApiUrl);
        if (response.ok) {
            const data = await response.json();


            if (data === undefined) {
                return null;
            }

            setCustomers(data.customers)
        }
    }
    useEffect(() => {
        fetchCustomers();
    }, [])

    const handleOnClick = () => {
    navigate("/customers/create");
}

    return (
        <>
        <div>
          <h1>Customer List</h1>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>First name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone Number</th>
          </tr>
        </thead>
        <tbody>
          {customer.map(customer => {
            return (
              <tr key={ customer.id }>
                <td>{ customer.first_name }</td>
                <td>{ customer.last_name }</td>
                <td>{ customer.address }</td>
                <td>{ customer.phone_number }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div><button onClick={handleOnClick} type="button" className='btn btn-primary' data-bs-toggle="button">add a new customer?</button></div>
      </div>
      </>
    );
  }

export default CustomerList;
