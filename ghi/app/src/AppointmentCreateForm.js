import React, { useEffect, useState } from 'react';

function AppointmentCreateForm() {
    const [tech, setTech] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        date_time: '',
        reason: '',
        customer: '',
        status: '',
        technician_id: '',
    });

    const [successMessage, setSuccessMessage] = useState('');
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        try {
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setTech(data.technicians);
            } else {
                console.error('Failed to fetch technician:', response.statusText);
            }
        } catch (error) {
            console.error('Error during fetch:', error.message);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const appointmentUrl = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify({
                ...formData,
                technician_id: formData.technician_id,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            const initialFormData = {
                vin: '',
                date_time: '',
                reason: '',
                customer: '',
                status: '',
                technician_id: '',
            };
            setFormData(initialFormData);


            setSuccessMessage('Appointment created successfully');
            setTimeout(() => {
                setSuccessMessage('');
            }, 3000);
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        if (inputName === "technician") {
            setFormData((prevFormData) => ({
                ...prevFormData,
                technician_id: value,
            }));
        } else {
            setFormData((prevFormData) => ({
                ...prevFormData,
                [inputName]: value,
            }));
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Appointment</h1>
                    {successMessage && <div className="alert alert-success">{successMessage}</div>}
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.vin} placeholder="VIN" required type="text" maxLength={17} name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vehicle Identification Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.date_time} placeholder="Date Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">Date Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                                <option value="">Choose a Technician</option>
                                {tech && tech.map(tech => (
                                    <option key={tech.id} value={tech.id}>
                                        {`${tech.first_name}, ${tech.last_name}`}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="status" id="status" value={formData.status} className="form-select">
                                <option value="">Status</option>
                                <option value="scheduled">Scheduled</option>
                                <option value="finished">Finished</option>
                                <option value="canceled">Canceled</option>
                            </select>
                        </div>
                        <button className="btn btn-success">Create Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentCreateForm;
