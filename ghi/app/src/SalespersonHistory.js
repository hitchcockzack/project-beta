import React, {useState, useEffect} from 'react';

function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalesPeople] = useState([]);
    const [selected, setSelected] = useState('');

    const onChange = (event) => {
        const value = event.target.value;
        setSelected(value);
    }

    const fetchSalesPeople = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(salespeopleUrl);

        if(response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople)
        }
    }

    const fetchSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';

        const response = await fetch(salesUrl)

        if(response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    useEffect(() => {
        fetchSales();
        fetchSalesPeople();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                    <div className="mb-3">
                        <select className="form-select" value={selected} onChange={onChange}>
                        <option value=''>Choose a Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    { salesperson.first_name } { salesperson.last_name }
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            {selected === ''
                                ? <tr><td colSpan="4">Please select a salesperson.</td></tr>
                                : (() => {
                                    const filteredSales = sales ? sales.filter(sale => selected == sale.salesperson.id) : [];
                                    return filteredSales.length > 0
                                        ? filteredSales.map(sale => (
                                            <tr key={ sale.id }>
                                                <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                                                <td>{ sale.customer.first_name } { sale.customer.last_name}</td>
                                                <td>{ sale.automobile.vin }</td>
                                                <td>${ sale.price }</td>
                                            </tr>
                                        ))
                                        : <tr><td colSpan="4">The selected salesperson has no recorded sales at CarCar.</td></tr>
                                })()}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalespersonHistory;
